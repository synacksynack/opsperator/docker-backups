apiVersion: v1
kind: Template
labels:
  app: backups
  template: backups-jenkins-pipeline
metadata:
  annotations:
    description: Kubernetes Backups - Jenkinsfile
    iconClass: icon-perl
    openshift.io/display-name: Kubernetes Backups CI
    tags: backups
  name: backups-jenkins-pipeline
objects:
- apiVersion: v1
  kind: BuildConfig
  metadata:
    annotations:
      description: Tests Backups images
    name: backups-jenkins-pipeline
  spec:
    strategy:
      jenkinsPipelineStrategy:
        jenkinsfile: |-
          def gitCommitMsg = ''
          def templateMark = 'backups-jenkins-ci'
          def templateSel  = 'jenkins-ci-mark'
          pipeline {
              agent {
                  node { label 'maven' }
              }
              options { timeout(time: 75, unit: 'MINUTES') }
              parameters {
                  string(defaultValue: 'master', description: 'Backups Docker Image - Source Git Branch', name: 'backupsBranch')
                  string(defaultValue: 'master', description: 'Backups Docker Image - Source Git Hash', name: 'backupsHash')
                  string(defaultValue: '${GIT_SOURCE_HOST}/${GIT_REPOSITORY}', description: 'Backups Docker Image - Source Git Repository', name: 'backupsRepo')
                  string(defaultValue: '3', description: 'Max Retry', name: 'jobMaxRetry')
                  string(defaultValue: '1', description: 'Retry Count', name: 'jobRetryCount')
                  string(defaultValue: '${OPENSHIFT_ROUTED_DOMAIN}', description: 'CI Router Root Domain', name: 'rootDomain')
              }
              stages {
                  stage('pre-cleanup') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      echo "Using project: ${openshift.project()}"
                                      echo "cleaning up previous assets for backups-${params.backupsHash}"
                                      openshift.selector("buildconfigs", [ "${templateSel}": "${templateMark}-${params.backupsHash}" ]).delete()
                                  }
                              }
                          }
                      }
                  }
                  stage('create') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      def namespace = "${openshift.project()}"
                                      try {
                                          timeout(10) {
                                              def cloneProto = "http"
                                              def created
                                              def objectsFromTemplate
                                              def privateRepo = false
                                              def repoHost = params.backupsRepo.split('/')[0]
                                              def templatePath = "/tmp/workspace/${namespace}/${namespace}-backups-jenkins-pipeline/tmpbackups${params.backupsBranch}/openshift"
                                              sh "git config --global http.sslVerify false"
                                              sh "rm -fr tmpbackups${params.backupsBranch}; mkdir -p tmpbackups${params.backupsBranch}"
                                              dir ("tmpbackups${params.backupsBranch}") {
                                                  try {
                                                      withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                          cloneProto = "https"
                                                          privateRepo = true
                                                          echo "cloning ${params.backupsRepo} over https, using ${repoHost} token"
                                                          try { git([ branch: "${params.backupsBranch}", url: "https://${GIT_TOKEN}@${params.backupsRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.backupsRepo}#${params.backupsBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      }
                                                  } catch(e) {
                                                      if (privateRepo != true) {
                                                          echo "caught ${e} - assuming no credentials required"
                                                          echo "cloning ${params.backupsRepo} over http"
                                                          try { git([ branch: "${params.backupsBranch}", url: "http://${params.backupsRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.backupsRepo}#${params.backupsBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      } else { throw e }
                                                  }
                                                  try {
                                                      gitCommitMsg = sh(returnStdout: true, script: "git log -n 1").trim()
                                                  } catch(e) { echo "In non-critical catch block resolving commit message - ${e}" }
                                              }
                                              try { sh "test -d ${templatePath}" }
                                              catch (e) {
                                                  echo "Could not find ./openshift in ${params.backupsRepo}#${params.backupsBranch}"
                                                  throw e
                                              }
                                              echo "Processing Backups:${params.backupsHash}, from ${repoHost}, tagging to ${params.backupsBranch}"
                                              try {
                                                  echo " == Creating ImageStream =="
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/imagestream.yaml")
                                                  echo "The template will create ${objectsFromTemplate.size()} objects"
                                                  created = openshift.apply(objectsFromTemplate)
                                                  created.withEach { echo "Created ${it.name()} with labels ${it.object().metadata.labels}" }
                                              } catch(e) { echo "In non-critical catch block while creating ImageStream - ${e}" }
                                              try {
                                                  echo " == Creating BuildConfigs =="
                                                  if (privateRepo) {
                                                      withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                          objectsFromTemplate = openshift.process("-f", "${templatePath}/build-with-secret.yaml",
                                                              '-p', "GIT_DEPLOYMENT_TOKEN=${GIT_TOKEN}", '-p', "BACKUPS_REPOSITORY_REF=${params.backupsHash}",
                                                              '-p', "BACKUPS_REPOSITORY_URL=${cloneProto}://${params.backupsRepo}")
                                                      }
                                                  } else {
                                                      objectsFromTemplate = openshift.process("-f", "${templatePath}/build.yaml",
                                                          '-p', "BACKUPS_REPOSITORY_REF=${params.backupsHash}",
                                                          '-p', "BACKUPS_REPOSITORY_URL=${cloneProto}://${params.backupsRepo}")
                                                  }
                                                  echo "The template will create ${objectsFromTemplate.size()} objects"
                                                  for (o in objectsFromTemplate) { o.metadata.labels["${templateSel}"] = "${templateMark}-${params.backupsHash}" }
                                                  created = openshift.apply(objectsFromTemplate)
                                                  created.withEach { echo "Created ${it.name()} from template with labels ${it.object().metadata.labels}" }
                                              } catch(e) { echo "In non-critical catch block while creating BuildConfigs - ${e}" }
                                          }
                                      } catch(e) {
                                          echo "In catch block while creating resources - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('build') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      try {
                                          timeout(25) {
                                              echo "watching backups-${params.backupsHash} docker image build"
                                              def builds = openshift.selector("bc", [ name: "backups-${params.backupsHash}" ]).related('builds')
                                              builds.untilEach(1) { return (it.object().status.phase == "Complete") }
                                          }
                                      } catch(e) {
                                          echo "In catch block while building Docker image - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('tag') {
                      steps {
                          script {
                              if ("${params.backupsBranch}" == "${params.backupsHash}") { echo "skipping tag - source matches target" }
                              else {
                                  openshift.withCluster() {
                                      openshift.withProject() {
                                          try {
                                              timeout(5) {
                                                  def namespace = "${openshift.project()}"
                                                  retry(3) {
                                                      sh """
                                                      oc login https://kubernetes.default.svc.cluster.local --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt --token=\$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) > /dev/null 2>&1
                                                      oc describe -n ${namespace} imagestreamtag backups:${params.backupsHash} || exit 1
                                                      oc tag -n ${namespace} backups:${params.backupsHash} backups:${params.backupsBranch}
                                                      """
                                                  }
                                              }
                                          } catch(e) {
                                              echo "In catch block while tagging Backups image - ${e}"
                                              throw e
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              post {
                  always {
                      script {
                          openshift.withCluster() {
                              openshift.withProject() {
                                  def namespace   = "${openshift.project()}"
                                  def postJobName = "${namespace}/${namespace}-post-triggers-jenkins-pipeline"
                                  currentBuild.description = """
                                  ${params.backupsRepo} ${params.backupsBranch} (try ${params.jobRetryCount}/${params.jobMaxRetry})
                                  ${gitCommitMsg}
                                  """.stripIndent()
                                  echo "cleaning up assets for backups-${params.backupsHash}"
                                  sh "rm -fr /tmp/workspace/${namespace}/${namespace}-backups-jenkins-pipeline/tmpbackups${params.backupsBranch}"
                                  openshift.selector("buildconfigs", [ "${templateSel}": "${templateMark}-${params.backupsHash}" ]).delete()
                                  def jobParams = [
                                          [$class: 'StringParameterValue', name: "jobMaxRetry", value: params.jobMaxRetry],
                                          [$class: 'StringParameterValue', name: "jobRetryCount", value: params.jobRetryCount],
                                          [$class: 'StringParameterValue', name: "jobStatus", value: currentBuild.currentResult],
                                          [$class: 'StringParameterValue', name: "sourceBranch", value: params.backupsBranch],
                                          [$class: 'StringParameterValue', name: "sourceComponent", value: "backups"],
                                          [$class: 'StringParameterValue', name: "sourceRef", value: params.backupsHash],
                                          [$class: 'StringParameterValue', name: "sourceRepo", value: params.backupsRepo]
                                      ]
                                  try { build job: postJobName, parameters: jobParams, propagate: false, wait: false }
                                  catch(e) { echo "caught ${e} starting Job post-process" }
                              }
                          }
                      }
                  }
                  changed { echo "changed?" }
                  failure { echo "Build failed (${params.jobRetryCount} out of ${params.jobMaxRetry})" }
                  success { echo "success!" }
                  unstable { echo "unstable?" }
              }
          }
      type: JenkinsPipeline
parameters:
- name: GIT_REPOSITORY
  description: Git Repostory URL, Relative to GIT_SOURCE_HOST
  displayName: Git Repository
  value: synacksynack/opsperator/docker-backups.git
- name: GIT_SOURCE_HOST
  description: Git FQDN we would build images from
  displayName: Git Server
  value: gitlab.com
