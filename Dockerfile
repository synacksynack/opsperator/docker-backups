FROM docker.io/debian:buster-slim

# Backups image for OpenShift Origin

LABEL io.k8s.description="Kubernetes Native Backups Image." \
      io.k8s.display-name="Kubernetes Backups" \
      io.openshift.tags="rsync,ldapsearch,psql,mysql-client,mongodb" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-backups" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0"

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    KUBE_RELURL=https://dl.k8s.io \
    KUBE_VERSION=v1.21.4 \
    MINIO_RELURL=https://dl.min.io/client/mc/release

COPY config/* /

RUN set -x \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
    && sed -i -e '/path-exclude \/usr\/share\/doc\/\*$/d' \
	/etc/dpkg/dpkg.cfg.d/docker \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Postgres12 Repository" \
    && apt-get -y install wget gnupg2 \
    && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc \
	| apt-key add - \
    && echo deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main \
	>/etc/apt/sources.list.d/postgres.list \
    && apt-get update \
    && echo "# Install Backups Utilities" \
    && apt-get -y install rsync ldap-utils default-mysql-client mongo-tools \
	wget bsd-mailx curl git libnss-wrapper dumb-init postgresql-client-12 \
	jq python-pip \
    && BKARCH=`uname -m` \
    && if ! echo "$BKARCH" | grep -E '(x86_64|i386|i686|armv7l|aarch64)' >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    elif test "$BKARCH" = x86_64; then \
	BKARCH=amd64; \
    elif test "$BKARCH" = i386 -o  "$BKARCH" = i686; then \
	BKARCH=i386; \
    elif test "$BKARCH" = armv7l; then \
	BKARCH=arm; \
    elif test "$BKARCH" = aarch64; then \
	BKARCH=arm64; \
    fi \
    && curl -fsSL -o kubernetes-client.tar.gz \
	$KUBE_RELURL/$KUBE_VERSION/kubernetes-client-linux-$BKARCH.tar.gz \
    && tar -C /usr/bin -xzf kubernetes-client.tar.gz \
	--strip-component 3 kubernetes/client/bin/kubectl \
    && wget $MINIO_RELURL/linux-$BKARCH/mc -O /usr/bin/mc \
    && mv /ldap.conf /etc/ldap/ \
    && echo "# Fixing Permissions" \
    && chmod +x /usr/bin/kubectl /usr/bin/mc \
    && for dir in /var/run /backups; \
	do \
	    mkdir -p "$dir" \
	    && chown -R 1001:root "$dir" \
	    && chmod -R u+rw "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge wget gnupg2 \
    && apt-get -y autoremove --purge \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
	kubernetes-client.tar.gz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

#TODO: cassandra backup not implemented!
#&& pip install cassandra-driver

USER 1001
WORKDIR /backups
ENTRYPOINT ["dumb-init","--","/run-backups.sh"]
