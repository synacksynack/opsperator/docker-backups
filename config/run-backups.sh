#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

BACKUP_INTERVAL=${BACKUP_INTERVAL:-3600}
BACKUPS_ROOT=/backups
DO_PRUNE=${DO_PRUNE:-yes}
DROP_FAULTY_BACKUPS=${DROP_FAULTY_BACKUPS:-false}
NOTIFY_ADDRESS=${NOTIFY_ADDRESS:-''}
NOTIFY_ENDPOINT=${NOTIFY_ENDPOINT:-''}
OPENLDAP_BACKUP_OPERATIONAL_ATTRIBUTES=${OPENLDAP_BACKUP_OPERATIONAL_ATTRIBUTES:-yes}
PRUNE_KEEP_AT_LEAST=${PRUNE_KEEP_AT_LEAST:-3}
PRUNE_USAGE_THRESHOLD=${PRUNE_USAGE_THRESHOLD:-85}
PRUNE_TARGET_THRESHOLD=${PRUNE_TARGET_THRESHOLD:-50}
S3_ACCESS_KEY=${S3_ACCESS_KEY:-}
S3_EXPORT_METHOD=${S3_EXPORT_METHOD:-rolling}
S3_REMOTE_ADDRESS=${S3_REMOTE_ADDRESS:-}
S3_REMOTE_BUCKET=${S3_REMOTE_BUCKET:-backups}
S3_REMOTE_PORT=${S3_REMOTE_PORT:-8080}
S3_REMOTE_PROTO=${S3_REMOTE_PROTO:-http}
S3_REMOTE_REGION=${S3_REMOTE_REGION:-us-east-1}
S3_SECRET_KEY=${S3_SECRET_KEY:-}
SECRET_DIR=${SECRET_DIR:-/run/secrets/kubernetes.io/serviceaccount}
WATCH_NAMESPACE=${WATCH_NAMESPACE:-default}
WATCH_OBJECTS=${WATCH_OBJECTS:-deployments,statefulsets,daemonsets}

if ! test -s "$SECRET_DIR/token"; then
    echo "CRITICAL: could not read k8s token"
    exit 1
fi

K8S_TOKEN=`cat "$SECRET_DIR/token"`
K8S_CMD="kubectl --token=$K8S_TOKEN --insecure-skip-tls-verify -n $WATCH_NAMESPACE"

if test -z "$SSH_OPTS"; then
    SSH_OPTS="-o StrictHostKeyChecking=no -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null"
fi
if test "`id -u`" -ne 0; then
    echo Setting up nswrapper
    (
	cat /etc/passwd
	echo "idoexist:x:`id -u`:`id -g`::$BACKUPS_ROOT:/bin/sh"
    ) >/tmp/backups-passwd
    (
	cat /etc/group
	if ! grep :`id -g`: /etc/group >/dev/null 2>/dev/null; then
	    echo "believe:x:`id -g`:"
	fi
    ) >/tmp/backups-group
    export HOME=$BACKUPS_ROOT
    export NSS_WRAPPER_PASSWD=/tmp/backups-passwd
    export NSS_WRAPPER_GROUP=/tmp/backups-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi

backupLdap()
{
    local dir output

    dir="$BACKUPS_ROOT/$OPENLDAP_HOST/$BACKUP_MARK"
    output=base.ldif
    mkdir -p "$dir"
    if echo "$OPENLAP_BACKUP_OPERATIONAL_ATTRIBUTES" \
	    | grep -iE '(yes|true)' >/dev/null; then
	LDAPTLS_REQCERT=never ldapsearch \
	    -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ \
	    -E pr=2147483647/noprompt \
	    -D "$OPENLDAP_BIND_DN" \
	    -w "$OPENLDAP_BIND_PW" \
	    -b "$OPENLDAP_BASE" + "*"
    else
	LDAPTLS_REQCERT=never ldapsearch \
	    -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ \
	    -E pr=2147483647/noprompt \
	    -D "$OPENLDAP_BIND_DN" \
	    -w "$OPENLDAP_BIND_PW" \
	    -b "$OPENLDAP_BASE"
    fi >"$dir/$output"
    if test -s "$dir/$output"; then
	echo successfully backed up $OPENLDAP_PROTO:$OPENLDAP_HOST
	post_process "$dir" "$output"
	return 0
    elif test "$DROP_FAULTY_BACKUPS" = true; then
	rm -fr "$dir"
    else
	mv "$dir" "$dir.failed"
    fi
    return 1
}

backupMongo()
{
    local dir output

    dir="$BACKUPS_ROOT/$MONGO_HOST/$BACKUP_MARK"
    output=$MONGO_DB.mongo-dump
    rm -fr "$dir"
    mkdir -p "$dir/$output"
    mongodump --host "$MONGO_HOST" --username "$MONGO_USER" \
	--password "$MONGO_PASS" --port "$MONGO_PORT" --db "$MONGO_DB" \
	--forceTableScan -o "$dir/$output"
    if ls -1 "$dir/$output" | grep [a-z] >/dev/null; then
	echo successfully backed up $MONGO_HOST:$MONGO_DB
	post_process "$dir" "$output"
	return 0
    elif test "$DROP_FAULTY_BACKUPS" = true; then
	rm -fr "$dir"
    else
	mv "$dir" "$dir.failed"
    fi
    return 1
}

backupMysql()
{
    local dir output

    dir="$BACKUPS_ROOT/$MYSQL_HOST/$BACKUP_MARK"
    output="$MYSQL_DB.sql"
    mkdir -p "$dir"
    mysqldump "-u$MYSQL_USER" "-p$MYSQL_PASS" "-h$MYSQL_HOST" "-P$MYSQL_PORT" \
	"$MYSQL_DB" >"$dir/$output"
    if test -s "$dir/$output"; then
	echo successfully backed up mysql:$MYSQL_HOST:$MYSQL_DB
	post_process "$dir" "$output"
	return 0
    elif test "$DROP_FAULTY_BACKUPS" = true; then
	rm -fr "$dir"
    else
	mv "$dir" "$dir.failed"
    fi
    return 1
}

backupPostgres()
{
    local dir output

    dir="$BACKUPS_ROOT/$PSQL_HOST/$BACKUP_MARK"
    output="$PSQL_DB.sql"
    mkdir -p "$dir"
    PGPASSWORD="$PSQL_PASS" pg_dump -U "$PSQL_USER" -h "$PSQL_HOST" \
	-p "$PSQL_PORT" "$PSQL_DB" >"$dir/$output"
    if test -s "$dir/$output"; then
	echo successfully backed up postgres:$PSQL_HOST:$PSQL_DB
	post_process "$dir" "$output"
	return 0
    elif test "$DROP_FAULTY_BACKUPS" = true; then
	rm -fr "$dir"
    else
	mv "$dir" "$dir.failed"
    fi
    return 1
}

backupRsync()
{
    local dir

    dir="$BACKUPS_ROOT/$SSH_HOST/$BACKUP_MARK"
    test -d "$HOME/.ssh" || mkdir -p "$HOME/.ssh"
    chmod 0750 "$HOME/.ssh"
    if test "$SSH_KEY_RAW"; then
	echo "$SSH_KEY_RAW" >"$HOME/.ssh/id_rsa"
	chmod 0600 "$HOME/.ssh/id_rsa"
    fi
    mkdir -p "$dir"
    rm -f $HOME/.ssh/known_hosts
    if rsync -aqWxP --rsh="ssh $SSH_OPTS -p$SSH_PORT" \
	    --usermap=:`id -u` --groupmap=:`id -g` \
	    "$SSH_USER@$SSH_HOST:$SSH_PATH/" "$dir/"; then
	echo successfully backed up rsync:$SSH_HOST:$SSH_PATH
	post_process "$dir" .
	return 0
    elif test "$DROP_FAULTY_BACKUPS" = true; then
	rm -fr "$dir"
    else
	mv "$dir" "$dir.failed"
    fi
    return 1
}

backupS3()
{
    local dir hostvar

    dir="$BACKUPS_ROOT/$S3_HOST-$S3_BUCKET/$BACKUP_MARK"
    hostvar=MC_HOST_$S3_ALIAS
    eval "$hostvar=$S3_PROTO://$S3_AK:$S3_SK@$S3_HOST:$S3_PORT"
    export $hostvar
    if ! mc ls $S3_ALIAS >/dev/null 2>&1; then
	echo failed querying $S3_HOST s3 endpoint >&2
	return 1
    elif ! mc ls $S3_ALIAS/$S3_BUCKET >/dev/null 2>&1; then
	echo bucket $S3_HOST/$S3_BUCKET not found >&2
	return 1
    fi
    mkdir "$dir"
    if mc cp --recursive "$S3_ALIAS/$S3_BUCKET" "$dir/"; then
	echo successfully backed up s3:$S3_HOST:$S3_BUCKET
	post_process "$dir" .
	return 0
    elif test "$DROP_FAULTY_BACKUPS" = true; then
	rm -fr "$dir"
    else
	mv "$dir" "$dir.failed"
    fi
    return 1
}

export_backup()
{
    (
	export MC_HOST_backups="$S3_REMOTE_PROTO://$S3_ACCESS_KEY:$S3_SECRET_KEY@$S3_REMOTE_ADDRESS:$S3_REMOTE_PORT"
	if ! mc ls backups >/dev/null 2>&1; then
	    echo failed querying s3 endpoint >&2
	    return 1
	elif ! mc ls backups/$S3_REMOTE_BUCKET >/dev/null 2>&1; then
	    if ! mc mb backups/$S3_REMOTE_BUCKET --region=$S3_REMOTE_REGION >/dev/null 2>&1; then
		echo failed creating remote bucket >&2
		return 1
	    else
		echo notice: created missing bucket $S3_REMOTE_BUCKET
	    fi
	fi
	awk '{print $2}' "$BACKUPS_ROOT/index.success" | \
	    while read servicename
	    do
		if test "$S3_EXPORT_METHOD" = rolling; then
		    TARGET_PATH=backups/$S3_REMOTE_BUCKET/$servicename/
		else #full
		    TARGET_PATH=backups/$S3_REMOTE_BUCKET/$servicename/$BACKUP_MARK/
		fi
		if ! mc cp --recursive "$BACKUPS_ROOT/$servicename/$BACKUP_MARK/" $TARGET_PATH; then
		    echo failed uploading $servicename/$BACKUP_MARK assets to s3 >&2
		else
		    echo done uploading $servicename/$BACKUP_MARK assets to s3
		fi
	    done
    ) && return 0
    return 1
}

get_secret_key()
{
    local value

    value=`$K8S_CMD get secret "$1" -o json 2>/dev/null | jq -r ".data[\"$2\"]" 2>/dev/null`
    if ! echo "$value" | grep '^null$' >/dev/null; then
	echo "$value" | base64 --decode 2>/dev/null
    fi
}

log_title()
{
    cat <<EOF

######################################
#####################################
##
## $@
##

EOF
}

match_objects()
{
    WDAY=`date +%w`
    HOUR=`date +%H`
    MIN=`date +%M`
    # global sleep time set to 1h: can't honor preferred time
    # running hourly backups. should otherwise add that
    # selector: backups.opsperator.io/preferredtime=$MIN
    # besides, I'm not sure how to best group those, considering we
    # can't guanrantee a job would start every BACKUP_INTERVAL
    $K8S_CMD get "$WATCH_OBJECTS" \
	--selector=backups.opsperator.io/interval=hourly \
	-o=jsonpath="{range .items[*]}{.kind}{' '}{.metadata.name}{'\n'}{end}" \
	2>/dev/null

    if test "$DEBUG_ALL"; then
	$K8S_CMD get "$WATCH_OBJECTS" \
	    --selector=backups.opsperator.io/interval=daily \
	    -o=jsonpath="{range .items[*]}{.kind}{' '}{.metadata.name}{'\n'}{end}"
	$K8S_CMD get "$WATCH_OBJECTS" \
	    --selector=backups.opsperator.io/interval=weekly \
	    -o=jsonpath="{range .items[*]}{.kind}{' '}{.metadata.name}{'\n'}{end}"
	return 0
    fi 2>/dev/null
    $K8S_CMD get "$WATCH_OBJECTS" \
	--selector=backups.opsperator.io/interval=daily \
	--selector=backups.opsperator.io/preferredtime=$HOUR \
	-o=jsonpath="{range .items[*]}{.kind}{' '}{.metadata.name}{'\n'}{end}" \
	2>/dev/null
    if test "$HOUR" = 2; then
	$K8S_CMD get "$WATCH_OBJECTS" \
	    --selector=backups.opsperator.io/interval=weekly \
	    --selector=backups.opsperator.io/preferredtime=$WDAY \
	    -o=jsonpath="{range .items[*]}{.kind}{' '}{.metadata.name}{'\n'}{end}" \
	    2>/dev/null
    fi
}

post_process()
{
    local dir input

    if test -z "$1"; then
	return 1
    elif test "$2"; then
	dir="$1"
	input="$2"
    else
	dir="$BACKUPS_ROOT"
	input="$1"
    fi
    if test -d "$dir/$input"; then
	return 0
    elif test -s "$dir/$input"; then
	ls -l "$dir/$input"
	if ! gzip -9 -f "$dir/$input"; then
	    echo WARNING: Failed compressing "$dir/$input"
	fi
    else
	echo "WARNING: empty $dir/$input" >&2
    fi
}

prune_older_backups()
{
    local total cpt pct

    if test `df -h "$BACKUPS_ROOT" | awk '/^\//{print $5}' | sed 's|%||'` \
	    -ge "$PRUNE_USAGE_THRESHOLD" 2>/dev/null; then
	df -h "$BACKUPS_ROOT" >/tmp/backups-usage.orig
	ls "$BACKUPS_ROOT/" | grep -v global-summary.log \
	    | while read servicename
		do
		    total=`ls "$BACKUPS_ROOT/$servicename/" | awk 'END{print NR}'`
		    if ! test "0$total" -gt 0; then
			echo "PRUNE: invalid total amount of backups found in $BACKUPS_ROOT/$servicename" >&2
			continue
		    fi
		    cpt=1
		    ls -tr1 "$BACKUPS_ROOT/$servicename" \
			| while read item
			    do
				pct=`expr $cpt '*' 100 / $total`
				if ! test "0$pct" -ge 0; then
				    echo "PRUNE: invalid percentage counter occured with $BACKUPS_ROOT/$servicename" >&2
				    break
				elif test `expr $total - $cpt` -le "$PRUNE_KEEP_AT_LEAST"; then
				    echo "PRUNE: aborting $BACKUPS_ROOT/$servicename/$item prune, keeping at least $PRUNE_KEEP_AT_LEAST backups"
				    break
				elif test "$pct" -le "$PRUNE_TARGET_THRESHOLD"; then
				    echo "PRUNE: purging $BACKUPS_ROOT/$servicename/$item"
				    rm -fr "$BACKUPS_ROOT/$servicename/$item"
				fi
				cpt=`expr $cpt + 1`
			    done
		    echo "PRUNE: done with $BACKUPS_ROOT/$servicename"
		done
	(
	    echo ======================
	    echo === NOTICE: disk usage
	    df -h "$BACKUPS_ROOT" >/tmp/backups-usage
	    echo PRUNE: started from
	    cat /tmp/backups-usage.orig
	    echo ""
	    echo PRUNE: finished with
	    cat /tmp/backups-usage
	    echo ""
	    echo ======================
	) | tee -a "$BACKUPS_ROOT/global-summary.log"
	rm -f /tmp/backups-usage*
    fi
}

echo "Backup interval is $BACKUP_INTERVAL seconds"
while :
do
    starttime=$(date +%s)
    export BACKUP_MARK=$(date +%Y%m%d%H%M%S)
    rm -f $BACKUPS_ROOT/index.failed $BACKUPS_ROOT/index.success
    match_objects | while read objKind objName
	do
	    echo NOTICE: processing discovered object $objKind/$objName
	    driver=`$K8S_CMD get $objKind $objName -o=jsonpath="{.metadata.labels.backups\.opsperator\.io\/driver}" 2>/dev/null`
	    secret=`$K8S_CMD get $objKind $objName -o=jsonpath="{.metadata.labels.backups\.opsperator\.io\/secret}" 2>/dev/null`
	    service=`$K8S_CMD get $objKind $objName -o=jsonpath="{.metadata.labels.backups\.opsperator\.io\/service}" 2>/dev/null`
	    if test -z "$service"; then
		# by default, the name of the deployment/sts object
		# matches that of its service
		serviceShort=$objName
		service=$serviceShort.$WATCH_NAMESPACE.svc.cluster.local
	    else
		# assume Backup deployment could be done in a third-party
		# namespace, force relying on FQDN querying target service
		serviceShort=$service
		service=$serviceShort.$WATCH_NAMESPACE.svc.cluster.local
	    fi
	    if test -z "$secret"; then
		# assume that by default, a given deployment backup secret
		# is named after that deployment
		secret=$objName
	    fi
	    backupMethod=
	    case "$driver" in
		ldap|openldap)
		    OPENLDAP_BASE=`get_secret_key "$secret" directory-root`
		    OPENLDAP_BIND_DN=cn=syncrepl,ou=services,$OPENLDAP_BASE
		    OPENLDAP_BIND_PW=`get_secret_key "$secret" syncrepl-password`
		    OPENLDAP_HOST=$service
		    OPENLDAP_PORT=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].port}' 2>/dev/null`
		    OPENLDAP_PROTO=`$K8S_CMD get $objKind $objName -o=jsonpath="{.metadata.labels.backups\.opsperator\.io\/proto}" 2>/dev/null`
		    if test -z "$OPENLDAP_PROTO"; then
			if test "$OPENLDAP_PORT" = 636 \
				-o "$OPENLDAP_PORT" = 1636; then
			    OPENLDAP_PROTO=ldaps
			else
			    OPENLDAP_PROTO=ldap
			fi
		    fi
		    if test -z "$OPENLDAP_PORT"; then
			OPENLDAP_PORT=389
		    fi
		    if test "$OPENLDAP_BASE" -a "$OPENLDAP_BIND_DN" -a "$OPENLDAP_BIND_PW"; then
			backupMethod=backupLdap
		    else
			echo "WARNING: Missing inputs, base:$OPENLDAP_BASE, dn:$OPENLDAP_BIND_DN, pw:$OPENLAP_BIND_PW"
		    fi
		    ;;
		mongo|mongodb)
		    MONGO_DB=`get_secret_key "$secret" database-name`
		    MONGO_HOST=$service
		    MONGO_PASS=`get_secret_key "$secret" database-password`
		    MONGO_PORT=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].port}' 2>/dev/null`
		    MONGO_USER=`get_secret_key "$secret" database-user`
		    if test -z "$MONGO_PORT"; then
			MONGO_PORT=27017
		    fi
		    if test "$MONGO_DB" -a "$MONGO_PASS" -a "$MONGO_USER"; then
			backupMethod=backupMongo
		    fi
		    ;;
		mariadb|mysql|percona)
		    MYSQL_DB=`get_secret_key "$secret" database-name`
		    MYSQL_HOST=$service
		    MYSQL_PASS=`get_secret_key "$secret" database-password`
		    MYSQL_PORT=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].port}' 2>/dev/null`
		    MYSQL_USER=`get_secret_key "$secret" database-user`
		    if test -z "$MYSQL_PORT"; then
			MYSQL_PORT=3306
		    fi
		    if test "$MYSQL_DB" -a "$MYSQL_PASS" -a "$MYSQL_USER"; then
			backupMethod=backupMysql
		    fi
		    ;;
		postgres)
		    PSQL_DB=`get_secret_key "$secret" database-name`
		    PSQL_HOST=$service
		    PSQL_PASS=`get_secret_key "$secret" database-password`
		    PSQL_PORT=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].port}' 2>/dev/null`
		    PSQL_USER=`get_secret_key "$secret" database-user`
		    if test -z "$PSQL_PORT"; then
			PSQL_PORT=5432
		    fi
		    if test "$PSQL_DB" -a "$PSQL_PASS" -a "$PSQL_USER"; then
			backupMethod=backupPostgres
		    fi
		    ;;
		rsync|ssh)
		    SSH_HOST=$service
		    SSH_KEY_RAW=`get_secret_key "$secret" id_rsa`
		    SSH_PATH=`$K8S_CMD get $objKind $objName -o=jsonpath="{.metadata.labels.backups\.opsperator\.io\/path}" 2>/dev/null`
		    SSH_PORT=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].port}' 2>/dev/null`
		    SSH_USER=`get_secret_key "$secret" username`
		    if test -z "$SSH_PATH"; then
			SSH_PATH=/backups
		    fi
		    if test -z "$SSH_USER"; then
			SSH_USER=backup
		    fi
		    if test -z "$SSH_PORT"; then
			SSH_PORT=22
		    fi
		    if test "$SSH_PATH" -a "$SSH_KEY_RAW"; then
			backupMethod=backupRsync
		    fi
		    ;;
		s3)
		    S3_ALIAS=$serviceShort
		    S3_AK=`get_secret_key "$secret" access-key`
		    S3_BUCKET=`get_secret_key "$secret" bucket`
		    S3_HOST=$service
		    S3_PORT=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].port}' 2>/dev/null`
		    S3_PROTO=`$K8S_CMD get svc $serviceShort -o=jsonpath='{.spec.ports[0].protocol}' 2>/dev/null`
		    S3_SK=`get_secret_key "$secret" secret-key`
		    if test -z "$S3_PORT"; then
			S3_PORT=8080
		    fi
		    if test -z "$S3_PROTO"; then
			if test "$S3_PORT" = 443 -o "$S3_PORT" = 8443; then
			    S3_PROTO=https
			else
			    S3_PROTO=http
			fi
		    fi
		    if test "$S3_AK" -a "$S3_SK" -a "$S3_HOST" -a "$S3_BUCKET"; then
			if ! echo "$S3_REMOTE_ADDRESS" | grep -E "($S3_HOST|$S3_ALIAS)" >/dev/null; then
			    backupMethod=backupS3
			elif ! test "$S3_BUCKET" = "$S3_REMOTE_BUCKET"; then
			    backupMethod=backupS3
			else
			    echo WARNING: ignoring $S3_HOST/$S3_BUCKET
			    echo WARNING: would later be used exporting backups >&2
			fi
		    fi
		    ;;
		*)
		    if test -z "$driver"; then
			echo "WARNING: skipping $objKind/$objName, driver not found"
		    else
			echo "WARNING: skipping $objKind/$objName, unsupporter driver '$driver'"
		    fi
		    ;;
	    esac
	    if test "$backupMethod"; then
		log_title Backing Up $driver:$service
		if ! $backupMethod; then
		    if test "$DROP_FAULTY_BACKUPS" = true; then
			touch "$BACKUPS_ROOT/$service/$BACKUP_MARK.failed"
		    fi
		    echo $driver $service >>"$BACKUPS_ROOT/index.failed"
		else
		    echo $driver $service >>"$BACKUPS_ROOT/index.success"
		fi
	    fi
	done
    if echo "$DO_PRUNE" | grep -iE '(yes|true)' >/dev/null; then
	prune_older_backups
    fi
    subj=NOTICE
    if test -s "$BACKUPS_ROOT/index.failed"; then
	if test -s "$BACKUPS_ROOT/index.success"; then
	    (
		echo The following were properly backed up:
		cat "$BACKUPS_ROOT/index.success"
		echo On the other hands, those have failed:
		cat "$BACKUPS_ROOT/index.failed"
	    ) | tee -a "$BACKUPS_ROOT/global-summary.log"
	    subj=WARNING
	else
	    (
		echo All backups have failed:
		cat "$BACKUPS_ROOT/index.failed"
	    ) | tee -a "$BACKUPS_ROOT/global-summary.log"
	    subj=CRITICAL
	fi
    elif ! test -s "$BACKUPS_ROOT/index.success"; then
	subj=MUTE
    else
	(
	    echo All backups have succeeded:
	    cat "$BACKUPS_ROOT/index.success"
	) | tee -a "$BACKUPS_ROOT/global-summary.log"
    fi
    if test "$subj" = CRITICAL; then
	echo "SKIPPED exporting to s3: all backups have failed!" \
	    | tee -a "$BACKUPS_ROOT/global-summary.log"
    elif test "$subj" = MUTE; then
	echo "SKIPPED exporting to s3: nothing new to upload" \
	    | tee -a "$BACKUPS_ROOT/global-summary.log"
    elif test "$S3_REMOTE_ADDRESS" -a "$S3_REMOTE_BUCKET" \
	    -a "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" \
	    -a "$S3_REMOTE_BUCKET" -a "$S3_REMOTE_PROTO"; then
	log_title "Exporting to s3:$S3_REMOTE_ADDRESS/$S3_REMOTE_BUCKET"
	if ! export_backup; then
	    echo "FAILED exporting backup to s3!" \
		| tee -a "$BACKUPS_ROOT/global-summary.log"
	else
	    echo "SUCCESS exporting to s3" \
		| tee -a "$BACKUPS_ROOT/global-summary.log"
	fi
    fi
    if test "$subj" = MUTE; then
	echo discarding empty backup log
	if test "$DEBUG"; then
	    cat "$BACKUPS_ROOT/global-summary.log"
	fi
	rm -f "$BACKUPS_ROOT/global-summary.log"
    else
	if test "$NOTIFY_ADDRESS"; then
	    cat "$BACKUPS_ROOT/global-summary.log" | mail \
		-s "[Backups][k8s/$WATCH_NAMESPACE] $subj" $NOTIFY_ADDRESS
	fi
	if test "$NOTIFY_ENDPOINT"; then
	    aggreg="`cat "$BACKUPS_ROOT/global-summary.log" | tr '\n' , | sed 's|,$||'`"
	    test -z "$NOTIFY_FMT" && NOTIFY_FMT='{"text":"%s"}'
	    NOTIFY_STR="$(echo "$NOTIFY_FMT" | sed "s|%s|$aggreg|")"
	    curl -s --data-urlencode "payload=$NOTIFY_STR"
	fi
	mv "$BACKUPS_ROOT/global-summary.log" \
	    "$BACKUPS_ROOT/global-summary.last"
    fi
    endtime=$(date +%s)
    sleeptime=$(expr $BACKUP_INTERVAL + $starttime - $endtime)
    if test "$sleeptime" -lt 0; then
	#sleeptime=$(expr $sleeptime '*' -1)
	#BACKUP_INTERVAL=$(expr $BACKUP_INTERVAL + $sleeptime + 120)
	#sleeptime=$BACKUP_INTERVAL
	#echo "missed last backup, as current job took to long"
	#echo "raising backup interval to $BACKUP_INTERVAL seconds"
	echo "missed last backup, as current job took to long"
	echo "starting right away ..."
	sleeptime=10
    fi >&2
    echo "INFO: sleeping for $sleeptime seconds until next job"
    sleep $sleeptime
done

echo "CRITICAL: main loop interrupted, now exiting" >&2
exit 1
