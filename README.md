# k8s Backups

Generic image discovering and backing up Kubernetes hosted services.

Build with:

```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                          |    Description                      | Default                                     |
| :---------------------------------------- | :---------------------------------- | :------------------------------------------ |
|  `BACKUP_INTERVAL`                        | Backup Interval in seconds          | `3600`                                      |
|  `BACKUPS_ROOT`                           | Backups Storage Root Directory      | `/backups`                                  |
|  `DO_PRUNE`                               | Backup Jobs should Prune Older Data | `yes`                                       |
|  `DROP_FAULTY_BACKUPS`                    | Purges Faulty Backups               | `false`                                     |
|  `NOTIFY_ADDRESS`                         | Notify Email Address                | undef                                       |
|  `NOTIFY_ENDPOINT`                        | Notify HTTP-Post Endpoint           | undef                                       |
|  `NOTIFY_FMT`                             | Notify HTTP-Post Format             | `{"text":"%s"}`                             |
|  `OPENLDAP_BACKUP_OPERATIONAL_ATTRIBUTES` | Backups LDAP Operational Attributes | `yes`                                       |
|  `PRUNE_KEEP_AT_LEAST`                    | Backups Prune Minimum Retention     | `3` backups                                 |
|  `PRUNE_USAGE_THRESHOLD`                  | Backups Prune Disk Usage Threshold  | `85`% full                                  |
|  `PRUNE_TARGET_THRESHOLD`                 | Backups Prune Target Threshold      | `50`% drop                                  |
|  `S3_ACCESS_KEY`                          | S3 Access Key                       | undef                                       |
|  `S3_EXPORT_METHOD`                       | S3 Exports Method                   | `rolling`, overwrites previous copy         |
|  `S3_REMOTE_ADDRESS`                      | S3 Service Host Address             | undef                                       |
|  `S3_REMOTE_BUCKET`                       | S3 Customer Bucket                  | `backups`                                   |
|  `S3_REMOTE_PORT`                         | S3 Service Port                     | `8080`                                      |
|  `S3_REMOTE_PROTO`                        | S3 Service Proto                    | `http`                                      |
|  `S3_REMOTE_REGION`                       | S3 Region - creating bucket         | `us-east-1`                                 |
|  `S3_SECRET_KEY`                          | S3 Access Secret Key                | undef                                       |
|  `SECRET_DIR`                             | Kubernetes Access Token Path        | `/run/secrets/kubernetes.io/serviceaccount` |
|  `WATCH_NAMESPACE`                        | Kubernetes Namespace to watch       | `default`                                   |
|  `WATCH_OBJECTS`                          | Objets to Watch for Backups labals  | `deployments,statefulsets,daemonsets`       |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point      | Description                                            |
| :----------------------- | ------------------------------------------------------ |
|  `/backups`              | Backups Root Directory                                 |
|  `$HOME/.ssh`            | Using Git, Private Keys cloning and pushing to remotes |

Backups to Minio
-----------------

Which should also be valid for other s3 implementations (to be confirmed, beware
of multiple API versions).

First, create a service user (should produce an access key and secret key pair)
and a private bucket, granting our user read/write privileges.

Set the `MINIO_*` variables accordingly.

Notifications
--------------

Configuring Slack notifications, we'ld set the following:

 - `NOTIFY_ENDPOINT=https://hooks.slack.com/services/xxx/yyy`

For email notifications, we'ld use instead:

 - `NOTIFY_ADDRESS=backups@example.com`

Scheduling Backups
-------------------

We may add the following labels, for our Backups deployment to process a given
Kubernetes hosted service:

```
apiVersion: xx
kind: yy
metadata:
  labels:
    backups.opsperator.io/driver: mysql
    backups.opsperator.io/interval: hourly
    backups.opsperator.io/preferredtime: "15" #runs every hour, around 15th minute
[or]
    backups.opsperator.io/driver: postgres
    backups.opsperator.io/interval: weekly
    backups.opsperator.io/preferredtime: "0" #runs on Sundays
[or]
    backups.opsperator.io/driver: ldap
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "2" #runs around 2:00 am
    backups.opsperator.io/proto: ldaps #optional, defaults to ldap
[or]
    backups.opsperator.io/driver: rsync
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "5"
[or]
    backups.opsperator.io/driver: s3
    backups.opsperator.io/interval: daily
    backups.opsperator.io/preferredtime: "7"
[optionally]
    backups.opsperator.io/secret: k8s-secret-with-service-credentials
    backups.opsperator.io/service: ldap-service-name #defaults to .metadata.name
```

The port to query running our backup would then be deduced from the Service
whose name matches our backed-up object (Deployment, StatefulSet, ...) OR, the
`backups.opsperator.io/service` label value, if such was set.

Backups Layout
---------------

The backups root would be at `/backups`, unless otherwise specified (via the
`BACKUPS_ROOT` environment variable).

That folder would hold one sub-folder per backed-up service.

Those sub-folder would hold backups, marked with a date (folder named after
backup date, `YYMMDDHHMMSS` formatted).

```
$ find . | grep -vE '\.kube|lost+found'
./backups-gitea-kube.ci.svc.cluster.local
./gitea-postgres-kube.ci.svc.cluster.local
./gitea-postgres-kube.ci.svc.cluster.local/20200630131206
./gitea-postgres-kube.ci.svc.cluster.local/20200630131206/dbudo.sql.gz
./index.success
./global-summary.last
./index.failed
./matomo-mariadb-kube.ci.svc.cluster.local
./matomo-mariadb-kube.ci.svc.cluster.local/20200630131206
./matomo-mariadb-kube.ci.svc.cluster.local/20200630131206/dbsr1.sql.gz
./openldap-kube.ci.svc.cluster.local
./openldap-kube.ci.svc.cluster.local/20200630131206
./openldap-kube.ci.svc.cluster.local/20200630131206/base.ldif.gz
./sonarqube-postgres-kube.ci.svc.cluster.local
./sonarqube-postgres-kube.ci.svc.cluster.local/20200630131206
./sonarqube-postgres-kube.ci.svc.cluster.local/20200630131206/db2va.sql.gz
```

A prune job would run, at the end of each backup iteration. Should disk
usage reach is limits (see `PRUNE_USAGE_THRESHOLD`, the disk usage threshold
triggering a purge), then we would wipe older backups, making sure we would
keep at least `PRUNE_KEEP_AT_LEAST` jobs per backed up service, and at least
`PRUNE_TARGET_THRESHOLD`% of a given service backups history.

To avoid disk saturations, and in general, it would be recommended to push your
backups out of Kubernetes. Here, we could use MinIO. See `MINIO_` prefixed
environment variables, passing the backup container with proper endpoint address
and credentials.
